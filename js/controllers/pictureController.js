app.controller('pictureController', function( $scope, $rootScope, Picture ){
	 	
	$rootScope.loading = true;
	$scope.pictures = [];
	Picture.allPictures().then( 
		function( response ){
			$rootScope.loading = false;
			$scope.pictures = response.data;
		},
		function(msg){
			console.log(msg);
		}
	);
});
