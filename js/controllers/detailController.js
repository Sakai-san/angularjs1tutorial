app.controller( 'detailController', function( $scope, $rootScope, $routeParams, Picture ){
	$rootScope.loading = true;
	$scope.picture = Picture.getPicture($routeParams.id).then( 
		function(pic){
			$rootScope.loading = false;
			$scope.picture = pic;
		},
		function(msg){
			console.log(msg);
		}
	);

});