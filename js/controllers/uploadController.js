app.controller( 'uploadController', function( $scope, $rootScope, Picture ){

	$scope.fileData = null;
	$scope.message = null;
	$scope.uploadSucceeded = false;

	$scope.uploadFile = function(){
		$rootScope.loading = true;
		Picture.addPicture( $scope.fileData ).then(
			function( response ){
				$rootScope.loading = false;
				if( response.success === true ){
					$scope.uploadSucceeded = true;
					$scope.message = "Image properly uploaded !"; 
					
					Picture.pictures.data.push( response.data );
				}
				else{
					$scope.uploadSucceeded = false;
					$scope.message = "Image not uploaded !"; 
				}
			},
			function(){
				$scope.uploadSucceeded = false;
				$scope.message = "Network issue !";
			}
		);
	}
});
