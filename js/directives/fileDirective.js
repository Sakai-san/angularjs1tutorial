app.directive('ngFile', function(){
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			element.on('change', function(){
				// scope.fileData = element[0].files[0]; // bind with the parent scope
				scope.$apply( function(){ scope.fileData = element[0].files[0]  }); // notify the watcher about the change
			});
		}
	}
}); 
