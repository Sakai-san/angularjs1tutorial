app.factory('Picture', function( $http, $q ){
	var factory = {
		pictures : false, // cache the promise
		allPictures : function(){
			var deferred = $q.defer();
			
			// already requested
			if( factory.pictures !== false ){
				deferred.resolve( factory.pictures );
			}
			else{
				$http.post("http://studybyyourself.com/wp-admin/admin-ajax.php?action=get_pictures")
				.success(function(data, status){
						factory.pictures = data;
						deferred.resolve( factory.pictures );
				})
				.error(function(data, status){
						deferred.reject("Network problem.");
				});
			}
			return deferred.promise;			
		},		
		getPicture : function(id){
			var deferred = $q.defer();
			var picture = {};
			factory.allPictures().then(
				function( d ){
					angular.forEach( d.data, function(value, key){
						if( value.id == id ){
							picture = value;
						}
					});
				deferred.resolve( picture );
				},
				function(msg){
					deferred.reject(msg);
			});
			return deferred.promise;
		},
		addPicture : function( file ){
			var fd = new FormData();
			fd.append('file', file);

			var deferred = $q.defer();		
			$http({
				method: 'post',
				url: "http://studybyyourself.com/wp-admin/admin-ajax.php?action=picture_upload",
				data : fd,
				headers : {'Content-Type': undefined}				
			})
			.success(function(data, status){
					deferred.resolve( data );
			})
			.error(function(data, status){
					deferred.reject("Network problem.");
			});
			return deferred.promise;
		}

	};
	
	return factory;
});
